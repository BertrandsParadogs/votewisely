import json
import requests

from apikey import apikey
import demjson
import bills_table_2 as bt
import reps_table2 as rt
import issues_table_2 as it
import table_api as sql

table_name = "reps_issue_bills"
table_schema = {
    "rep_id": ("CHAR(10)", "NOT NULL"),
    "rep_name": ("CHAR(40)", "NOT NULL"),
    "issue": ("TEXT", "NOT NULL"),
    "count": ("INT", "NOT NULL", "DEFAULT 0"),
}
foreign_keys = {"rep_id": f"{rt.table_name}({rt.primary_key})"}


def get_all_bills(issue_topic):
    offset = 0
    issue_bills = []
    res = bt.get_request(issue_topic, offset)
    while len(res) > 0 and offset < 1000:
        issue_bills += res
        offset += 20
        if offset < 1000:
            res = bt.get_request(issue_topic, offset)
    return issue_bills


def insert_bills_count_by_reps():
    issues = sql.sql_fetch_all(f"SELECT {it.primary_key}, name FROM {it.table_name}")
    reps_to_bills = {}
    reps_to_names = {}
    for issue, name in issues:
        bills = get_all_bills(name)
        bills_count_by_reps = {}
        counter = 0
        for bill in bills:
            rep_id = bill["sponsor_id"]
            rep_name = bill["sponsor_name"]
            has_rep = rep_id in bills_count_by_reps
            if not has_rep:
                has_rep = sql.sql_fetch_all(
                    f"SELECT {rt.primary_key} FROM {rt.table_name} "
                    f'WHERE {rt.primary_key}="{rep_id}"'
                )
            if has_rep:
                if not rep_id in reps_to_names:
                    reps_to_names[rep_id] = rep_name
                if not rep_id in bills_count_by_reps:
                    bills_count_by_reps[rep_id] = {}
                bills_count_by_reps[rep_id][issue] = (
                    bills_count_by_reps[rep_id].get(issue, 0) + 1
                )
                if not rep_id in reps_to_bills:
                    rep_bills = sql.sql_fetch_all(
                        f'SELECT {bt.primary_key} FROM {bt.table_name} WHERE sponsor_id="{rep_id}"'
                    )
                    reps_to_bills[rep_id] = set(rep_bill[0] for rep_bill in rep_bills)
                    if (
                        len(reps_to_bills[rep_id]) < 5
                        and bill["bill_id"] not in reps_to_bills[rep_id]
                    ):
                        bt.insert_bill(bill, issue)
                        reps_to_bills[rep_id].add(bill["bill_id"])
                counter += 1
        print(f"Accounted for {counter} out of {len(bills)} bills in issue {name}")
        for rep_id, stats in bills_count_by_reps.items():
            for issue, count in stats.items():
                sql.insert_table(
                    table_name,
                    table_schema,
                    {
                        "rep_id": rep_id,
                        "rep_name": reps_to_names[rep_id],
                        "issue": issue,
                        "count": count,
                    },
                )


if __name__ == "__main__":
    sql.create_table(table_name, table_schema, foreign_keys)
    insert_bills_count_by_reps()
