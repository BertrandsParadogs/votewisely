import mysql.connector
from mysql.connector import errorcode
import requests
import sys
import unittest
from unittest.mock import patch, Mock

sys.path.insert(0, ".")
import bills_table
from apikey import apikey


class BillsTableTests(unittest.TestCase):
    @patch("requests.get")
    def test_get_request_queries_correctly(self, mock_method):
        mock_response = Mock()
        mock_response.status_code = 200
        mock_response.json = lambda: {"test_key": "test_result"}
        mock_method.return_value = mock_response

        test_topic = "test_topic"
        test_res = bills_table.get_request(test_topic)

        self.assertEqual(test_res, mock_response.json())
        self.assertTrue(
            mock_method.called_once_with(
                url=f"https://api.propublica.org/congress/v1/bills/search.json?query={test_topic}&offset&0&sort=_score",
                header={"X-API-Key": apikey},
            )
        )

    @patch("requests.get")
    def test_get_request_raise_exception_on_error(self, mock_method):
        mock_response = Mock()
        mock_response.status_code = 404
        mock_response.json = lambda: {"test_key": "test_result"}
        mock_method.return_value = mock_response

        with self.assertRaises(Exception) as error:
            test_topic = "test_topic"
            test_res = bills_table.get_request(test_topic)
            self.assertEqual(error.msg, f"GET /tasks/ {mock_response.status_code}")

    @patch("bills_table.cursor.execute")
    def test_create_table_calls_cursor_execute(self, mock_method):
        bills_table.create_table("bills_table")
        self.assertEqual(mock_method.call_count, 3)

    @patch("bills_table.cursor.execute")
    def test_insert_data_calls_cursor_execute(self, mock_method):
        test_bill = {
            "bill_id": "fake",
            "issue_name": "issue",
            "number": "none",
            "bill_uri": "test",
            "title": "title",
            "sponsor_title": "what",
            "bill_type": "nope",
            "sponsor_id": "1234",
            "sponsor_name": "you want some",
            "sponsor_state": "useless",
            "sponsor_party": "china",
            "introduced_date": "2012-08-09",
            "committees": "why",
            "primary_subject": "nope",
            "summary": "okay",
            "summary_short": "omg",
            "congressdotgov_url": "help",
        }

        bills_table.insert_data([test_bill], "bills_table", "test")

        self.assertEqual(mock_method.call_count, 1)
        call_args = mock_method.call_args_list[0]
        for arg in call_args:
            if type(arg) == "dict":
                self.assertEqual(arg["issue_name"], "test")
