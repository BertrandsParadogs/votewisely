import json
import requests

from apikey import apikey
import demjson
import issues_table_2 as it
import table_api as sql

table_name = "bills_table_2"
table_schema = {
    "bill_id": ("CHAR(20)", "NOT NULL"),
    "number": ("CHAR(20)", "NOT NULL"),
    "bill_uri": ("CHAR(128)", "NOT NULL"),
    "title": ("TEXT", "NOT NULL"),
    "sponsor_title": ("CHAR(20)", "NOT NULL"),
    "bill_type": ("CHAR(20)", "NOT NULL"),
    "sponsor_id": ("CHAR(10)", "NOT NULL"),
    "sponsor_name": ("CHAR(64)", "NOT NULL"),
    "sponsor_state": ("CHAR(10)", "NOT NULL"),
    "sponsor_party": ("CHAR(10)", "NOT NULL"),
    "introduced_date": ("CHAR(20)", "NOT NULL"),
    "committees": ("TEXT", "NOT NULL"),
    "congressdotgov_url": ("VARCHAR(256)", "NOT NULL"),
    "primary_subject": ("CHAR(64)", "NOT NULL"),
    "summary_short": ("TEXT", "NOT NULL"),
    "latest_major_action_date": ("CHAR(20)", "NOT NULL"),
    "latest_major_action": ("TEXT", "NOT NULL"),
}
primary_key = "bill_id"
foreign_keys = {
    "sponsor_id": "congress_members(id)",
    "sponsor_state": "states_table(abbrev)",
}
table_constraints = {
    "bills_table_2_bill_id_uindex": f"UNIQUE ({primary_key})",
    "pk_bills_id": f"PRIMARY KEY ({primary_key})",
}

relation_table_name = "bills_to_issues"
relation_table_schema = {
    "bill_id": ("CHAR(20)", "NOT NULL"),
    "issue": ("VARCHAR(256)", "NOT NULL"),
}
relation_foreign_keys = {
    "bill_id": "bills_table_2(bill_id)",
    "issue": "issues_table_2(route)",
}


def get_request(issue_topic, offset=0):
    """
    Make GET API call to the ProPublica using API_KEY
    :return: JSON format of the request result
    """
    query_topic = issue_topic.replace(" ", "%20")
    query_topic = query_topic.replace('"', "%22")
    request_url = f"https://api.propublica.org/congress/v1/bills/search.json?query={query_topic}&offset={offset}"

    params = {"X-API-Key": apikey}
    res = requests.get(url=request_url, headers=params)
    if res.status_code != 200:
        raise Exception(f"GET /tasks/ {res.status_code}")
    else:
        print(
            f"API GET request with query {issue_topic} and offset {offset} successful!"
        )
    return demjson.decode(res.content)["results"][0]["bills"]


def insert_bill(bill_dict, issue):
    existing_bill = sql.sql_fetch_all(
        f"SELECT issue FROM {relation_table_name} "
        f"WHERE {primary_key}='{bill_dict[primary_key]}'"
    )
    if existing_bill:
        print(f"Duplicate {bill_dict[primary_key]} found")
        issues = [issue[0] for issue in existing_bill]
        if not issue in issues:
            sql.insert_table(
                relation_table_name,
                relation_table_schema,
                {"bill_id": bill_dict[primary_key], "issue": issue},
                None,
            )
        print("Updating bill...")
    for key in bill_dict:
        if type(bill_dict[key]) != str and type(bill_dict[key]) != int:
            bill_dict[key] = json.dumps(bill_dict[key])
    sql.insert_table(table_name, table_schema, bill_dict, primary_key)
    if not existing_bill:
        sql.insert_table(
            relation_table_name,
            relation_table_schema,
            {"bill_id": bill_dict[primary_key], "issue": issue},
            None,
        )


def insert_bills():
    """
    Insert the bill data into the database
    :return:
    """
    issues = sql.sql_fetch_all(f"SELECT {it.primary_key}, name FROM {it.table_name}")
    for issue, name in issues:
        bills = get_request(name)
        usable_bills = []
        for bill in bills:
            has_rep = sql.sql_fetch_all(
                f"SELECT id FROM congress_members WHERE id=\"{bill['sponsor_id']}\" AND full_name=\"{bill['sponsor_name']}\""
            )
            has_state = sql.sql_fetch_all(
                f"SELECT abbrev FROM states_table WHERE abbrev=\"{bill['sponsor_state']}\""
            )
            if has_rep and has_state:
                usable_bills.append(bill)
                insert_bill(bill, issue)
        print(f"Insertion {len(bills)} bills for {name}!")
        issue_bills = [
            usable_bills[i][primary_key] for i in range(min(5, len(usable_bills)))
        ]
        it.update_issue_bills(issue, issue_bills)
        print(f"Updated bills for {issue}!")


if __name__ == "__main__":
    sql.create_table(table_name, table_schema, foreign_keys, table_constraints)
    sql.create_table(relation_table_name, relation_table_schema, relation_foreign_keys)
    insert_bills()
