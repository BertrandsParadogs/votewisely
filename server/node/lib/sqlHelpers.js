function onConnectionClose(err) {
	if (err) {
		return console.log("error: " + err.message);
	}
	console.log("Closed the database connection.");
}

// Requires process_fn to be a closure that can take the result of a sql query as an argument.
function jsonResponseFactory(res, process_fn) {
	return function(error, results, fields) {
		if (error) {
			res.json({
				"status": 500,
				"error": error,
				"response": null
			});
		} else {
			if (typeof process_fn !== 'undefined')
				results = process_fn(results);
			res.json({
				"status": 200,
				"error": null,
				"response": results
			});
		}
	};
}

// Requires sql_fn to be a closure that can take the result of a backend request object from Express as an argument, and return a sql statement with its user arguments.
function sqlRequestFactory(sql_fn, process_fn) {
	return function(req, res) {
		const [sql, args] = sql_fn(req);
		res.locals.connection.query(sql, args, jsonResponseFactory(res, process_fn));
		res.locals.connection.end(onConnectionClose);
	}
}

module.exports = {
	sqlRequestFactory,
	jsonResponseFactory,
	onConnectionClose
};