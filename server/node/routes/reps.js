const express = require("express");
const helpers = require("../lib/sqlHelpers");
const router = express.Router();

const TABLE = "congress_members";

function repsSQLGeneratorFactory(type) {
	return function(req) {
		const args = [];
		var sql = "SELECT * "
			+ "FROM " + TABLE + " ";

		if (typeof req.query.id !== "undefined" || typeof req.query.state !== "undefined"
			|| typeof req.query.last_name !== "undefined" || typeof req.query.first_name !== "undefined") {
			sql += "WHERE "
			if (req.query.id) {
				sql += "id=? AND ";
				args.push(req.query.id);
			}
			if (req.query.state) {
				sql += "state=? AND ";
				args.push(req.query.state);
			}
			if (req.query.last_name) {
				sql += "last_name=? AND ";
				args.push(req.query.last_name);
			}
			if (req.query.first_name) {
				sql += "first_name=? AND ";
				args.push(req.query.first_name);
			}
			sql = sql.substring(0, sql.length-4);
		}
		if (type === "senate") {
			sql += "AND short_title='Sen.' ";
		} else if (type === "house") {
			sql += "AND short_title='Rep.' "
		}
		if (req.query.sort_by_last) {
			sql += " ORDER BY last_name";
		}
		return [sql, args];
	}
}

/**
 * Get the congress member information with various queries
 * empty: show all the member information
 * ?state="TX": show all the member information from Texas
 * ?last_name="Cruz": show all the member information with the last name "Cruz"
 * ?first_name="Ted": show all the member information with the first name "Ted"
 * ?id="A000360": show the member information with the id "A000360"
 * ?sort-by-last=true: sort the results by member's last name
 */
router.get("/", helpers.sqlRequestFactory(repsSQLGeneratorFactory()));


/**
 * Get the senate member information with various queries
 * empty: show all the member information
 * ?state="TX": show all the member information from Texas
 * ?last_name="Cruz": show all the member information with the last name "Cruz"
 * ?first_name="Ted": show all the member information with the first name "Ted"
 * ?id="A000360": show the member information with the id "A000360" 
 * ?sort-by-last=true: sort the results by member's last name
 */
router.get("/senate/", helpers.sqlRequestFactory(repsSQLGeneratorFactory("senate")));

/**
 * Get the house member information with various queries
 * empty: show all the member information
 * ?state="TX": show all the member information from Texas
 * ?last_name="Cruz": show all the member information with the last name "Cruz"
 * ?first_name="Ted": show all the member information with the first name "Ted"
 * ?id="A000360": show the member information with the id "A000360" 
 * ?sort-by-last=true: sort the results by member's last name
 */
router.get("/house/", helpers.sqlRequestFactory(repsSQLGeneratorFactory("house")));

module.exports = {
	repsSQLGeneratorFactory,
	router};