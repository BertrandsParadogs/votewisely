const express = require("express");
const helpers = require("../lib/sqlHelpers");
const router = express.Router();

const TABLE = "issues_table_2";

function issuesListSQLGenerator(req) {
	const args = [];
	var sql = "SELECT issues_table_2.route, name, icon, "
		+ "CONCAT(LEFT(summary, 100), '...') AS summary, "
		+ "question, votes.votes_total, "
		+ "votes_yes*100/votes.votes_total AS pct_yes, "
		+ "votes_no*100/votes.votes_total AS pct_no, "
		+ "(votes.votes_total - votes_yes - votes_no)*100/votes.votes_total AS pct_neutral, "
		+ "category "
		+ "FROM " + TABLE + " "
		+ "INNER JOIN ("
			+ "SELECT votes.route, CASE "
			+ "WHEN votes.votes_predict < votes.votes_total THEN votes.votes_total "
			+ "ELSE votes.votes_predict "
			+ "END AS votes_total "
			+ "FROM ("
				+ "SELECT route, (votes_yes + votes_no) AS votes_predict, "
				+ "votes_total "
				+ "FROM issues_table_2) "
			+ "AS votes) AS votes "
		+ "ON issues_table_2.route=votes.route;";
	sql += " SELECT DISTINCT category FROM " + TABLE + ";"
	return [sql, args];
}

function issuesListResultsProcessor(results) {
	results[1] = results[1].map(category => category.category);
	return results;
}

/* Returns a list of issue names */
/* Request example: /api/issues2/ */
router.get("/", helpers.sqlRequestFactory(issuesListSQLGenerator, issuesListResultsProcessor));

function issueInfoSQLGenerator(req) {
	const args = [req.params.issue];
	const sql = "SELECT * "
		+ "FROM " + TABLE + " "
		+ 'WHERE route=?;';
	return [sql, args];
}

function issueInfoResultsProcessor(results) {
	results = results[0];
	results["side_yes"] = JSON.parse(results["side_yes"]);
	results["side_no"] = JSON.parse(results["side_no"]);
	results["side_center"] = JSON.parse(results["side_center"]);
	return results;
}

/* Return information of an issue by name */
/* Request example: /api/issue2/abortion */
router.get("/:issue", helpers.sqlRequestFactory(issueInfoSQLGenerator, issueInfoResultsProcessor));

module.exports = {
	issuesListSQLGenerator,
	issuesListResultsProcessor,
	issueInfoSQLGenerator,
	issueInfoResultsProcessor,
	router
};