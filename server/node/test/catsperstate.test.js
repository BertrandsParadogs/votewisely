const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const catsperstate = require('../routes/catsperstate');


const TABLE = "states_issue_bills";
const ISSUES_TABLE = "issues_table_2";

describe("CatsPerState", () => {
	it("catsPerStateSQLGenerator should generate a SQL statement and its arguments", () => {
        var mockReq = sinon.mock();
        mockReq.params = sinon.mock();
        mockReq.params.state = 'TX';
        const [testSQL, testSQLargs] = catsperstate.catsPerStateSQLGenerator(mockReq);
        sql = "SELECT category, SUM(count) as count FROM (" + "SELECT issues.category, count "
        + "FROM " + TABLE + " "
        + "LEFT JOIN (SELECT name, category FROM " + ISSUES_TABLE + ") AS issues ON issues.name=" + TABLE + ".issue "
        + 'WHERE state=?'
    + ") AS state_stats GROUP BY category"
        expect(testSQL).to.equal(sql);
        expect(testSQLargs).to.have.length(1);
        expect(testSQLargs[0]).to.equal("TX");
    });
    
    it("statesSQLGenerator should generate a SQL statement", () => {
        var mockReq = sinon.mock();
        const [testSQL, testSQLargs] = catsperstate.statesSQLGenerator(mockReq);
        sql = "SELECT DISTINCT state FROM " + TABLE;
        expect(testSQL).to.equal(sql);
        expect(testSQLargs).to.have.length(0);
        expect(testSQLargs).to.be.empty;
	});

})