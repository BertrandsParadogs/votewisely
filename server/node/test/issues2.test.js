const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const issues2 = require('../routes/issues2');

describe("Issues", () => {
	it("issuesListSQLGenerator should generate a SQL statement and its arguments", () => {
		const [testSQL, testSQLargs] = issues2.issuesListSQLGenerator();
		expect(testSQL).to.equal("SELECT issues_table_2.route, name, icon, "
			+ "CONCAT(LEFT(summary, 100), '...') AS summary, "
			+ "question, votes.votes_total, "
			+ "votes_yes*100/votes.votes_total AS pct_yes, "
			+ "votes_no*100/votes.votes_total AS pct_no, "
			+ "(votes.votes_total - votes_yes - votes_no)*100/votes.votes_total AS pct_neutral, "
			+ "category "
			+ "FROM issues_table_2 "
			+ "INNER JOIN ("
				+ "SELECT votes.route, CASE "
				+ "WHEN votes.votes_predict < votes.votes_total THEN votes.votes_total "
				+ "ELSE votes.votes_predict "
				+ "END AS votes_total "
				+ "FROM ("
					+ "SELECT route, (votes_yes + votes_no) AS votes_predict, "
					+ "votes_total "
					+ "FROM issues_table_2) "
				+ "AS votes) AS votes "
			+ "ON issues_table_2.route=votes.route; SELECT DISTINCT category FROM issues_table_2;");
		expect(testSQLargs).to.be.empty;
	});

	it("issuesListResultsProcessor should map convert list of category objects to a list of categories", () => {
		const mockResults = [[sinon.mock()], [sinon.mock(), sinon.mock()]];
		mockResults[0][0].issue = "testIssue";
		mockResults[1][0].category = "testCategory1";
		mockResults[1][1].category = "testCategory2";
		const testResults = issues2.issuesListResultsProcessor(mockResults);
		expect(testResults).to.be.an('array');
		expect(testResults).to.have.length(2);
		expect(testResults[0]).to.be.an('array');
		expect(testResults[0]).to.have.length(1);
		expect(testResults[0][0].issue).to.equal("testIssue");
		expect(testResults[1]).to.be.an('array');
		expect(testResults[1]).to.have.length(2);
		expect(testResults[1][0]).to.equal("testCategory1");
		expect(testResults[1][1]).to.equal("testCategory2");
	});

	it("issueInfoSQLGenerator should generate a SQL statement and its arguments", () => {
		const mockRes = sinon.mock();
		mockRes.params = sinon.mock();
		mockRes.params.issue = "testIssue";
		const [testSQL, testSQLargs] = issues2.issueInfoSQLGenerator(mockRes);
		expect(testSQL).to.equal("SELECT * "
			+ "FROM issues_table_2 "
			+ "WHERE route=?;");
		expect(testSQLargs).to.be.an('array');
		expect(testSQLargs).to.have.length(1);
		expect(testSQLargs[0]).to.equal("testIssue");
	});

	it("issueInfoResultsProcessor should JSON parse strings for fields in results", () => {
		const mockResults = [sinon.mock()];
		mockResults[0]["side_yes"] = '[{"side": "testSide1", "pct": 1}]';
		mockResults[0]["side_no"] = '[{"side": "testSide2", "pct": 2}]';
		mockResults[0]["side_center"] = "[]";
		const testResults = issues2.issueInfoResultsProcessor(mockResults);
		expect(testResults).to.not.be.an('array');
		expect(testResults["side_yes"]).to.be.an('array');
		expect(testResults["side_yes"][0]).to.deep.equal({
			side: "testSide1",
			pct: 1
		});
		expect(testResults["side_no"]).to.be.an('array');
		expect(testResults["side_no"][0]).to.deep.equal({
			side: "testSide2",
			pct: 2
		});
		expect(testResults["side_center"]).to.be.empty;
	});
});