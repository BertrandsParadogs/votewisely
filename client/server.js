const compression = require("compression");
const express = require("express");
const favicon = require("express-favicon");
const fs = require("fs");
const helmet = require("helmet");
const https = require("https");
const morgan = require("morgan");
const path = require("path");

const app = express();

const APP_PORT = process.env.PORT || 8080;

app.use(helmet());
app.use(compression()); 

app.use(morgan("combined"));

// Add the favicon
app.use(favicon(path.join(__dirname, "build", "favicon.ico")));

// Serve the static files from the build folder
app.use(express.static(path.join(__dirname, "build")));

// Redirect all traffic to the index
app.get("*", function(req, res){
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

// Host https if there is certificate
try {
  https.createServer({
    key: fs.readFileSync(path.join(process.env.HOME, "certs", "server.key")),
    cert: fs.readFileSync(path.join(process.env.HOME, "certs", "server.crt"))
  }, app).listen(APP_PORT);
} catch(err) {
  console.log("Cannot find certificate for https, hosting on http");
  app.listen(APP_PORT);
}
console.log("Client server listening to port", APP_PORT);
