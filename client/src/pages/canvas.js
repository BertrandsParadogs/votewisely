import React, {PureComponent} from 'react';
import {Container} from 'react-bootstrap';
import RepsBubbleChart from '../components/repsBubbleChart'

export default class Canvas extends PureComponent {
	render() {
		return (
			<Container>
				<div>
					<h1>Canvas</h1>
				</div>
				<RepsBubbleChart />
			</Container>
		);
	}
}
