import GridListTile from '@material-ui/core/GridListTile';
import {shallow, mount} from 'enzyme';
import React from 'react';

import Banner from '../../components/banner';

import Politicians from '../politicians_table';

import MUIDataTable from 'mui-datatables';


describe("Representatives Directory", () => {

	it("successfully renders without crashing", () => {
		const wrapper = shallow(<Politicians />);
		expect(
			wrapper.length
		).toEqual(1);
	});

	it("renders representatives", () => {
		const wrapper = mount(<Politicians />);
		/* MUIDataTable moved into renderTable method */
		// expect(wrapper.find(MUIDataTable).exists()).toEqual(true);
		// expect(wrapper.find(MUIDataTable).prop("title")).toEqual("Politicians");
		expect(wrapper.find("img").prop("src")).toEqual("https://countercurrents.org/wp-content/uploads/2016/06/US-PRESIDENTIAL-ELECTION-0F-2016-9.jpg");
	});
});
