import React, { PureComponent } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import MemberCard from '../components/memberCard';
import '../css/about.css';

/* Member Images */
import ProfileJezze from '../imgs/members/jezze_gif.gif';
import ProfileLisa from '../imgs/members/lisa_gif.gif';
import ProfileAmiti from '../imgs/members/amiti.jpg';
import ProfileJay from '../imgs/members/jay.jpeg';
import ProfileFan from '../imgs/members/fan.png';
import ProfileJesus from '../imgs/members/jesus_gif.gif';

var commits = [];
var issues = {};

/* GUI */

class Purpose extends React.PureComponent {
  render() {
    return (
      <div id="purpose-container">
        <h2 className="title-text">Our Mission</h2>
        <p id="mission-statement">
          "Our Mission is to help citizens be politically informed
          and get excited to vote by providing them with unbiased
          information on politicians and issues based on their location"
        </p>
      </div>
    );
  }
}

class MemberList extends PureComponent {
	render() {
		return(
			<Container id="members-list" fluid={true}>
        <h2 className="title-text">Meet the Engineers</h2>
        <Row noGutters={false}>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Jesse Martinez"
              job="Front-end Engineer"
              class="5th-Year CS Student"
              bio="Really into gaming and all things 'geek'.
                   Still not over Endgame and
                   Episode 3 of Season 8 of Game of Thrones"
              image={ProfileJezze}
              commits={133}
              issues={79}
              tests={12}
            />
          </Col>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Lisa Barson"
              job="Front-end Engineer"
              class="Junior CS Student"
              bio="Enjoys to play volleyball and go hiking!"
              image={ProfileLisa}
              commits={73}
              issues={27}
              tests={12}
            />
          </Col>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Amiti Busgeeth"
              job="Front-end Engineer"
              class="Junior CS Student"
              bio="Cats and kittens are adorable!"
              image={ProfileAmiti}
              commits={45}
              issues={31}
              tests={10}
            />
          </Col>
        </Row>
        <Row noGutters={false}>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Jie Hao Liao"
              job="Back-end Engineer"
              class="Junior CS Student"
              bio="Loving that Chipotle Lobster. And K-Pop. More K-Pop"
              image={ProfileJay}
              commits={138}
              issues={59}
              tests={24}
            />
          </Col>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Fan Yang"
              job="Back-end Engineer"
              class="Junior CS Student"
              bio="People say he can even be seen around campus riding on his 'Hoverboard'....
                   What a guy."
              image={ProfileFan}
              commits={36}
              issues={35}
              tests={84}
            />
          </Col>
          <Col xs={12} md={4} className="even-columns">
            <MemberCard
              name="Jesus Vasquez"
              job="Back-end Engineer"
              class="Senior CS Student"
              bio="Game of Thrones is life. That is pretty much it."
              image={ProfileJesus}
              commits={33}
              issues={21}
              tests={14}
            />
          </Col>
        </Row>
      </Container>
		);
	}
}

class Stats extends PureComponent {
  constructor(props) {
    super(props);
    this.contributors = {
      "fanyang": {
        name: "Fan Yang",
        username: "BertrandsParadogs",
        unitTests: 84
      },
      "JesusVasq": {
        name: "Jesus Vasquez",
        username: "JesusVasq",
        unitTests: 14
      },
      "AmitiB": {
        name: "Amiti Busgeeth",
        username: "AmitiB",
        unitTests: 10
      },
      "Lisa Barson": {
        name: "Lisa Barson",
        username: "lisab",
        unitTests: 12
      },
      "Jesse Martinez": {
        name: "Jezze Martinez",
        username: "Jezze4",
        unitTests: 12
      },
      "Jie Hao Liao": {
        name: "Jie Hao Liao",
        username: "liaojh1998",
        unitTests: 24
      }
    };
    this.state = {
      done: false
    };
  }

  componentDidMount() {
    fetch("https://gitlab.com/api/v4/projects/11046118/repository/contributors")
      .then(res => res.json())
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          const contributor = res[i];
          const data = {};
          data.name = this.contributors[contributor.name].name;
          data.username = this.contributors[contributor.name].username;
          data.commits = contributor.commits;

          commits[i] = data.commits;
          console.log("commits Username: " + data.name + " has " + data.commits + " commits");

          const state = {};
          state[data.username] = data;
          this.setState(state);
        }
        return res;
      })
      .then(res => {
        this.setState({ done: true });
        for (let i = 0; i < res.length; i++) {
          const contributor = res[i];
          const username = this.contributors[contributor.name].username;
          fetch("https://gitlab.com/api/v4/users?username=" + username)
            .then(res => res.json())
            .then(res => res[0].id)
            .then(id => {
              fetch("https://gitlab.com/api/v4/projects/11046118/issues?assignee_id=" + id)
                .then(res => {
                  const data = Object.assign({}, this.state[username]);
                  data.issues = Number(res.headers.get('X-Total'));

                  issues[username] = data.issues;

                  const state = {};
                  state[username] = data;
                  this.setState(state);
                });
            });
        }
      });
  }

  totalCommits() {
    const commits = Object.values(this.contributors).reduce(
      (total, contributor) => total + this.state[contributor.username].commits, 0);
    return commits;
  }

  totalIssues() {
    if (Object.values(this.contributors).every((contributor) =>
        this.state[contributor.username].issues > 0)) {
      const issues = Object.values(this.contributors).reduce(
        (total, contributor) => total + this.state[contributor.username].issues, 0);
      return issues;
    }
    return 0;
  }

  totalUnitTests(){
    var total = 0;
    for (var key in this.contributors){
      var user = this.contributors[key];
      total = total + user.unitTests;
    }
    return total;
}

  renderTable() {
    if (this.state.done) {
      return (
        <Container>
          <table className="table table-cover">
            <thead>
              <tr>
                <th scope="col">Names</th>
                <th scope="col">Commits</th>
                <th scope="col">Assigned Issues</th>
                <th scope="col">Unit Tests</th>
              </tr>
            </thead>
            <tbody>
              {Object.values(this.contributors).map((contributor, idx) => (
                <tr className={"about-stat-row-" + idx}>
                  <td className="about-stat">{this.state[contributor.username].name}</td>
                  <td className="about-stat">{this.state[contributor.username].commits}</td>
                  <td className="about-stat">{this.state[contributor.username].issues}</td>
                  <td className="about-stat">{contributor.unitTests}</td>
                </tr>
              ))}
              <tr className="about-stat-row">
                <td className="about-stat"><strong>Total</strong></td>
                <td className="about-stat">{this.totalCommits()}</td>
                <td className="about-stat">{this.totalIssues()}</td>
                <td className="about-stat">{this.totalUnitTests()}</td>
              </tr>
            </tbody>
          </table>
        </Container>
      );
    }
    return (
      <div>
        <h3>Loading...</h3>
      </div>
    );
  }

  render() {
    return (
      <Container id="stats-container">
        <h2 className="title-text">The Statistics</h2>
        {this.renderTable()}
      </Container>
    );
  }
}


class Sources extends PureComponent {
	render(){
		return(
				<Container id="sources-container">
					<h2 className="title-text">Our Toolbelt</h2>
					<Row>
						<Col xs={6} md={4}>
							<a href="https://gitlab.com/votewiselyengineers/votewisely">
								<img
									src="https://about.gitlab.com/images/press/logo/png/gitlab-logo-gray-stacked-rgb.png"
									alt="GitLab"
									className="logo"
								/>
							</a>
						</Col>
						<Col xs={6} md={4}>
							<img
								src="https://cdn.dribbble.com/users/1238764/screenshots/5864612/slack-animation.gif"
								alt="Slack"
								className="logo"
							/>
						</Col>
						<Col xs={6} md={4}>
							<img
								src="https://www.import.io/wp-content/uploads/2017/10/React-logo-1.png	"
								alt="React"
								className="logo"
							/>
						</Col>
						<Col xs={6} md={4}>
							<img
								src="https://thumbs.gfycat.com/PoliticalMindlessBanteng-small.gif"
								alt="AWS"
								className="logo"
							/>
						</Col>
						<Col xs={6} md={4}>
							<img
								src="http://movingmindz.com/wp-content/uploads/2018/04/photoshop-full-logo.png"
								alt="Photoshop"
								className="logo"
							/>
						</Col>
						<Col xs={6} md={4}>
							<img
								src="https://www.python.org/static/community_logos/python-logo-generic.svg"
								alt="Python"
								className="logo"
							/>
						</Col>
						<Col xs={6} md={4}>
							<img
								src="https://fuzati.com/wp-content/uploads/2016/12/Bootstrap-Logo.png"
								alt="Bootstrap"
								className="logo"
							/>
						</Col>
						<Col xs={6} md={4}>
							<a href="https://documenter.getpostman.com/view/6804368/S11NMwfE">
								<img
									src="https://www.getpostman.com/img/v2/media-kit/Logo/PNG/pm-logo-vert.png"
									alt="Postman"
									className="logo"
								/>
							</a>
						</Col>
						<Col xs={6} md={4}>
							<img
								src="https://www.probytes.net/wp-content/uploads/2018/10/flask-logo-png-transparent.png"
								alt="Flask"
								className="logo"
							/>
						</Col>
					</Row>
				</Container>
		);
	}
}


export default class About extends PureComponent {
	render () {
		return (
			<Container id="about-container">
				<Purpose />
				<MemberList />
				<Stats />
				<Sources />
			</Container>
		);
	}
}

export { Purpose, MemberList, Stats, Sources };
