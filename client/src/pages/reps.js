import React, {Component} from 'react';

import { nodelink } from '../api';

function displayIds(ids) {
	var size = 10;
	return (
		ids.slice(0, size).map((rep, idx) =>
			<div key={idx}>
				Representative: {rep.first_name} {rep.last_name}, ID: {rep.id}<br />
				JSON: {JSON.stringify(rep)}
			</div>
		)
	);
}


var firstname ="";
var lastname ="";
var title ="";

export default class Reps extends Component {
	constructor(props){
		super(props);
		this.state = {
			ids: [],
			rep: {},
			single_state: {}
		}
	}

	componentDidMount() {
		fetch(nodelink + "/api/reps/ids?sort_by_last=true")
			.then(res => res.json())
			.then(res => this.setState({ ids: res.response }));
		fetch(nodelink + "/api/reps/id/A000374")
			.then(res => res.json())
			.then(res => res.response[0])
			.then(rep => this.setState({ rep: rep }));

		//FOR JEZZE, this gets the info for one state USING ABBREVATION
		var abbrev = "FL"
		fetch(nodelink + "/api/states/"+abbrev)
			.then(res => res.json())
			.then(res => res.response[0])
			.then(rep => this.setState({ single_state: rep }));
	}

	render () {
		return (
			<div>
				<h1>Welcome to Representatives...</h1>
				<h2>Request /api/reps/ids?sort_by_last=true example:</h2>
				<div>{displayIds(this.state.ids)}</div>
				<br />
				<h2>Request /api/reps/id/A000374 example:</h2>
				<h4>{this.state.rep.short_title} {this.state.rep.first_name} {this.state.rep.last_name}</h4>
				<div>Party: {this.state.rep.party === "R" ? "Republican" : "Democrat"}</div>
				<div>
					Facebook account:&nbsp;
						<a href={"https://www.facebook.com/" + this.state.rep.facebook_account} target="_blank" rel="noopener noreferrer">
							{this.state.rep.facebook_account}
						</a>
				</div>
				<div>
					Twitter account:&nbsp;
						<a href={"https://www.twitter.com/" + this.state.rep.twitter_account} target="_blank" rel="noopener noreferrer">
							{this.state.rep.twitter_account}
						</a>
				</div>
				<div>JSON: {JSON.stringify(this.state.rep, null, 2)}</div>
				<br />
				<h2>Other Requests</h2>
				<div>
					You may also request by first name and/or last name by the following request:<br/>
					/api/reps/name?first=Ralph&last=Abraham
				</div>


				<h2> FOR JEZZE USING STATES </h2>
				<p> DO the request in component did mount, and save that info using this.setState.
				I left an example there for Texas, and below here I show how to get that info</p>
				<div>ABBREVATION: {this.state.single_state.abbrev}</div>
				<div>NAME: {this.state.single_state.name}</div>
				<div>POPULATION: {this.state.single_state.population}</div>
				<div>BANNER_URL: {this.state.single_state.banner_url}</div>
				<div>LEGISLATURE NAME: {this.state.single_state.legislature_name}</div>

				<div>
				<div>also, if you need a list of ALL the states with all teir info, then just 
				remove the abbreviation from the API request, but you will have to unpack it differently</div>
				</div>

			</div>
		);
	}
}
