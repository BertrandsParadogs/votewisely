import React, {Component} from 'react';
import {Nav, Navbar} from 'react-bootstrap';
import Button from '@material-ui/core/Button'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { NavLink } from "react-router-dom";
import '../css/navbar.css';
import Image from '../imgs/favicon.ico';

export default class NavBar extends Component {

  constructor(props){
    super(props);
    this.state = {
      openNav: false
    };
  }

  toggleNavbar(open){
    this.setState({openNav: open});
  }

  render() {
    return (
      <div id="navbar-container">

        <Button id="navbar-icon"onClick={(e)=>this.toggleNavbar(true)}>
          <img
            src={Image}
            className="nav-icon-img glowing"
            style={{height: '80px', width: '80px', borderRadius:'50%'}}
            alt="|||"/>
        </Button>
        <SwipeableDrawer
          // anchor="top"
          open={this.state.openNav}
          classes={{paperAnchorLeft: 'mydrawer'}}
          onOpen={()=>this.toggleNavbar(true)}
          onClose={()=>this.toggleNavbar(false)}
          >
          <div
            tabIndex={0}
            role="button"
            onClick={()=>this.toggleNavbar(false)}
            onKeyDown={()=>this.toggleNavbar(false)}
            >
              <Button className="navbar-button">
                <div className="navbar-link" onClick={(e)=>this.toggleNavbar(false)}>
                  <i className="fas fa-2x fa-times"></i>
                </div>
              </Button><br /><br />
              <Nav>
                <NavLink exact activeClassName="nav-active" className="navbar-link" to="/">Home</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/issues">Issues</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/politicians">Politicians</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/states">States</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/map">Map</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/about">About</NavLink>
                <NavLink activeClassName="nav-active" className="navbar-link" to="/visualization">Visualization</NavLink>
              </Nav>
          </div>
        </SwipeableDrawer>

      {/* <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top" className="my-custom" id="nav-desk">
        <Navbar.Brand href="/">Vote Wisely</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link><Link to="/">Home</Link></Nav.Link>
            <Nav.Link><Link to="/issues">Issues</Link></Nav.Link>
            <Nav.Link><Link to="/politicians">Politicians</Link></Nav.Link>
            <Nav.Link><Link to="/states">States</Link></Nav.Link>
            <Nav.Link><Link to="/map">Map</Link></Nav.Link>
            <Nav.Link><Link to="/about">About</Link></Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar> */}

      {/* Mobile View Navbar*/}

      {/* <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="bottom" className="my-custom" id="nav-mobile">
        <Navbar.Brand href="/">Vote Wisely</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <NavLink exact activeClassName="nav-active" to="/">Home</NavLink>
            <NavLink activeClassName="nav-active" to="/issues">Issues</NavLink>
            <NavLink activeClassName="nav-active" to="/politicians">Politicians</NavLink>
            <NavLink activeClassName="nav-active" to="/states">States</NavLink>
            <NavLink activeClassName="nav-active" to="/map">Map</NavLink>
            <NavLink activeClassName="nav-active" to="/about">About</NavLink>
            <NavLink activeClassName="nav-active" to="/visualization2">Customer Data</NavLink>
          </Nav>
        </Navbar.Collapse>
      </Navbar> */}
      </div>
    );
  }
}
