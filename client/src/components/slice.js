import React, {PureComponent} from 'react';
import * as d3 from "d3";
import '../css/slice.css';

export default class Pie extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    var {fillColor, innerRadius, outerRadius, startAngle, endAngle, label} = this.props;
    var arc = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .startAngle(startAngle)
      .endAngle(endAngle);

    return (
      <g>
        <path d={arc()} fill= {this.props.fillColor}/>
        <text className="slice-text"
              transform={`translate(${arc.centroid(this.props)})`}
              textAnchor="middle"
              fill="white">
          {this.props.label}
        </text>
      </g>
    );
  }
}
