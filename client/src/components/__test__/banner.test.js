import React from 'react';
import { shallow } from 'enzyme';

import Banner from '../banner';

describe('Banner', () => {
	it('should render a banner without text on it', () => {
		const wrapper = shallow(<Banner img="../../imgs/about_banner.jpg"/>);
		expect(wrapper.find("img").props().src).toBe("../../imgs/about_banner.jpg");
		expect(wrapper.find("h1").text()).toEqual("");
	});

	it('should render a banner with text on it', () => {
		const wrapper = shallow(<Banner img="../../imgs/about_banner.jpg" text="test"/>);
		expect(wrapper.find("img").props().src).toBe("../../imgs/about_banner.jpg");
		expect(wrapper.find("h1").text()).toEqual("test");
	});
});
