import React, {PureComponent} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import TempImage from '../imgs/issues.jpeg';
import '../css/search.css';
import { Link } from 'react-router-dom';


/* Individual search card. Component within SearchResults
   4 Props:           ---------------------------------
    picture           | pic |   Title                 |
    title             |  ^  |   link                  |
    link              |  ^  |   snippet               |
    snippet           ---------------------------------
 */
export default class SearchCard extends PureComponent {

  /* Parsing when 'highlighting' was done in the backend
     divided into sections because spanning won't work as an Object*/

  parseSnippetOld(snippet, section){
    var sections = snippet.split('<');
    if(section === 1){
      return sections[0];
    }
    else if(section === 2){
      var subSection = sections[1].split('>')
      return subSection[1];
    }
    else if(section === 3){
      var subSection = sections[2].split('>')
      return subSection[1];
    }
    else {
      console.log("entered wrong section for snippets")
    }
  }

  /* Jay was messing with this */

  parseSnippet(snippet, search, index) {
    var terms = search.split(/\s+/gi).sort((a, b) => b.length - a.length);
    for (var i = 0; i < terms.length; i++) {
      var term = RegExp('(?=(' + terms[i] + '))', 'gi');
      snippet = snippet.replace(term, '<span class="hi-lite">');
      term = RegExp('(?<=(' + terms[i] + '))', 'gi');
      snippet = snippet.replace(term, '</span>');
    }
    console.log(snippet);
    return snippet;

    // var subIndex = snippet.toUpperCase().indexOf(search.toUpperCase());
    // if(index === 0)
    //   return snippet.slice(0, subIndex);
    // else if(index === 1)
    //   return snippet.slice(subIndex, subIndex+search.length);
    // else if(index === 2)
    //   return snippet.slice(subIndex+search.length);
    // else
    //   console.log("wrong index in hiliteCell.js, Line 19");
  }


  render() {
    return (
      <Container id="searchCard-container">
        <Row>
          {/* <Col md={2}>
            <img src={TempImage} style={{width:'100%', height:'100%'}} alt=""/>
          </Col> */}
          <Col md={12}>
            <Row>
              <h4><Link to={this.props.link}>{this.props.title}</Link></h4>
            </Row>
            <Row>
              <span style={{color:"green"}}>https://thewisevote.com{this.props.link}</span>
            </Row>
            <Row>
              <p style={{color:"gray"}}>
                {/* {this.parseSnippet(String(this.props.snippet), String(this.props.search), 0)} */}
                {/* <span className="hi-lite"> */}
                {/* {this.parseSnippet(String(this.props.snippet), String(this.props.search), 1)} */}
                {/* </span> */}
                <div dangerouslySetInnerHTML={{
                  __html : this.parseSnippet(String(this.props.snippet), String(this.props.search), 2)
                }}/>
              </p>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}
