import React from 'react';

export default function TwitterTimeline(props) {
  return (
    <div>
      <a className="twitter-timeline" data-width="300"
          data-height="400" data-theme="light"
          data-link-color="#2B7BB9" href=
          {"https://twitter.com/" + props.twitter_url + "?ref_src=twsrc%5Etfw"}>
        Tweets by {props.twitter_url}
      </a>
      <script async src="https://platform.twitter.com/widgets.js" charSet="utf-8"></script>
    </div>
  );
}