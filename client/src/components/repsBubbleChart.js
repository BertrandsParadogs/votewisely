import React, { PureComponent } from 'react';
import * as d3 from "d3";

import { nodelink } from '../api';
import '../css/repsBubbleChart.css';

export default class RepsBubbleChart extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    fetch(nodelink + '/api/issuesperrep')
      .then(res => res.json())
      .then(res => res.response)
      .then(res => this.setState({ data: res }))
  }

  componentDidUpdate() {
    if (this.state.data) {
      // Dimensions
      const width = "calc(100% - 40px)";
      const height = 600;
      const scaleLimit = 100;

      // Get svg DOM
      const svg = d3.select("#repsBubbleChart")
        .style("width", width)
        .style("height", height);

      // Create circles
      const dimensions = svg.node().getBoundingClientRect();
      const hierarchy = d3.hierarchy({ children: this.state.data })
        .sum(d => d.issues)
        .sort((a, b) => b.value - a.value);
      const pack = d3.pack()
        .size([dimensions.width, dimensions.height])
        .padding(2);
      const circlesProperties = pack(hierarchy);

      // Circles group configuration
      const circlesGroup = svg.append("g")
        .attr("id", "circles")
        .attr("transform", "translate(0,0) scale(1)");

      // Setup group for each circle
      const circles = circlesGroup.selectAll("g")
        .data(circlesProperties.leaves())
        .join("g");

      // Add the circles
      circles.append("circle")
        .attr("cy", d => d.y)
        .attr("cx", d => d.x)
        .attr("r", d => d.r)
        .attr("fill", d => d.data.party === "R" ? "red" : (d.data.party === "D" ? "blue" : "green"));

      // Update labels
      function updateLabels(transform, minRadius) {
        if (transform.k > minRadius) {
          circles.each(function(d) {
            const circle = d3.select(this);
            if (Math.abs(transform.k * d.x + transform.x - dimensions.width / 2) < dimensions.width / 2 + transform.k * d.r
                && Math.abs(transform.k * d.y + transform.y - dimensions.height / 2) < dimensions.height / 2 + transform.k * d.r) {
              if (circle.select("text").empty()) {
                // Add label to the circle
                const label = circle.append("text")
                  .attr("x", d => d.x)
                  .attr("y", d => d.y)
                  .attr("text-anchor", "middle");

                // Add politician link to label
                label.append("a")
                  .attr("href", d => "/politicians/" + d.data.rep_name.toLowerCase().replace(/\s+/g, '-'))
                  .style("font-size", d => d.r / d.data.rep_name.length * 3)
                  .text(d => d.data.rep_name);
                
                // Add issues covered by politician
                label.append("tspan")
                  .attr("x", d => d.x)
                  .attr("y", d => d.y + d.r / d.data.rep_name.length * 2.5)
                  .style("font-size", d => d.r / d.data.rep_name.length * 1.5)
                  .text(d => "Issues covered: " + d.data.issues);
              }
            } else {
              circle.select("text").remove();
            }
          })
        } else {
          circles.select("text").remove();
        }
      }

      // Dragging for the circles
      const circlesDragBehavior = d3.drag().on("start", () => {
        d3.event.on("drag", () => {
          const matrix = circlesGroup.node().transform.baseVal.consolidate().matrix;
          const s = matrix.a;
          const x = matrix.e;
          const y = matrix.f;
          circlesZoomBehavior.translateBy(svg, d3.event.dx / s, d3.event.dy / s);
          circlesGroup.attr("transform", "translate("
            + (x + d3.event.dx)
            + ","
            + (y + d3.event.dy)
            + ") scale(" + s + ")");
        });
      });
      svg.call(circlesDragBehavior)

      // Zooming for the circles
      const circlesZoomBehavior = d3.zoom().on("zoom", () => {
        const group = d3.select("#circles");
        const matrix = group.node().transform.baseVal.consolidate().matrix;
        const transform = d3.event.transform;
        group.attr("transform", "translate(" + transform.x + ","
          + transform.y + ") scale(" + transform.k + ")");
        updateLabels(transform, 8);
      })
      .filter(() => d3.event.type === "wheel")
      .scaleExtent([1, scaleLimit]);
      svg.call(circlesZoomBehavior)
        .on("wheel", () => { d3.event.preventDefault(); });

      // Draw the border for the SVG
      svg.append("rect")
        .attr("width", dimensions.width)
        .attr("height", dimensions.height)
        .attr("fill", "none")
        .attr("stroke", "black")
        .attr("stroke-width", 1);
    }
  }

  render() {
    if (this.state.data) {
      return (
        <div style={{background: "whitesmoke", paddingBottom: '40px', borderRadius: '10px', marginTop: '50px'}}>
            <br />
            <h5>Number of Issues Covered by the Bills of Each Politician</h5>
            <svg id="repsBubbleChart" />
        </div>
      );
    }
    return (
      <div style={{background: "whitesmoke", paddingBottom: '40px', borderRadius: '10px', marginTop: '50px'}}>
        <br />
        <h5>Number of Issues Covered by the Bills of Each Politician</h5>
        <h3>Loading...</h3>
      </div>
    );
  }
}
