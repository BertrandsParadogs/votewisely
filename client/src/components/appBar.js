import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import SearchBar from '../components/searchBar';
import Input from '@material-ui/core/Input';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import Fade from '@material-ui/core/Fade';
import '../css/appBar.css';

export default class MyAppBar extends PureComponent {

  static contextTypes = {
    router: PropTypes.object
  }

  render() {
    return(
      <div className="appbar-container">
        <AppBar position="fixed" classes={{root: 'appbar'}}>
          <Toolbar variant="dense">
            <SearchBar
              placeholder='Search the site'
              className='search-bar sb-global'
              searchFunction={(search)=>{
                this.context.router.history.push("/search/" + search);
              }}
            />
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
