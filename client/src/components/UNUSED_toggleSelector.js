import React, { PureComponent } from 'react';
import '../css/toggleSelector.css';

export default class ToggleSelector extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="custom-control custom-switch">
        <label class="toggleSelector-first">Sort by Alphabet: Descending</label>
        <input type="checkbox" class="custom-control-input" id="customSwitches"></input>
        <label class="custom-control-label" for="customSwitches">Ascending</label>
      </div>
    );
  }
}
