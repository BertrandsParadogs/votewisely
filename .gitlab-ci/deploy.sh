#!/bin/bash

mkdir .gitlab-ci/files-to-deploy
while read file; do
	if [[ -n $file ]] && [[ ${file::1} != "#" ]]; then
		mkdir -p ".gitlab-ci/files-to-deploy/$(dirname $file)"
		cp -r $file .gitlab-ci/files-to-deploy/$file
	fi
done < $1
