#!/bin/bash

cd ~/production/client && pm2 stop client-production && rm -rf build server.js \
	|| if [ $? -eq 1 ]; then echo "client-production not hosted"; fi
cd ~/production/server/node && pm2 stop server-production && rm -rf !\(package.json\|package-lock.json\|node_modules\) \
	|| if [ $? -eq 1 ]; then echo "client-production not hosted"; fi
